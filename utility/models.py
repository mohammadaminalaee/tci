from django.contrib.auth.models import User
from django.db import models



class Utility(models.Model):
    en_name = models.CharField(max_length=32)
    fa_name = models.CharField(max_length=32)
    purchase_callback = models.URLField()

    payment_types = models.ManyToManyField('payment.PaymentType')

    class Meta:
        verbose_name_plural = "Utilities"

    def __unicode__(self):
        return self.en_name

    def make_bill_transaction(self, username, bill, au=12345):
        from payment.models import UtilTransaction, PaymentType
        user = User.objects.get(username=username)
        payment_type = self.payment_types.first() # online only
        transaction = UtilTransaction.objects.create(util=self, amount=bill['Amount'],
                                        bill_id=bill['BillID'], pay_id=bill['PayID'],
                                        payment_type=payment_type, user=user, au=au)
        return transaction

    def make_adsl_transaction(self, username, number, amount, au):
        from payment.models import UtilTransaction, PaymentType
        user = User.objects.get(username=username)
        payment_type = self.payment_types.first() # online only
        transaction = UtilTransaction.objects.create(util=self, amount=amount,
                                        payment_type=payment_type, user=user, au=au)
        return transaction