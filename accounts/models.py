from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from utils import generate_activation_code, generate_activation_time, generate_password
from payment.utils import get_bill_info


class BaseAccountModel(models.Model):
    registration_date = models.DateTimeField(auto_now_add=True)
    activation_code = models.CharField(max_length=5, default=generate_activation_code)
    activation_code_expiration = models.DateTimeField(default=generate_activation_time)
    active = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)

    class Meta:
        abstract = True

    @property
    def is_active(self):
        return self.active and self.is_verified

    @property
    def is_verified(self):
        return self.verified

    def generate_activation_code(self):
        """
        Generating new activation code
        """
        self.activation_code = generate_activation_code()
        self.activation_code_expiration = generate_activation_time()
        self.save()
        return self.activation_code

    def verify_activation_code(self, activation_code):

        if self.activation_code != activation_code:
            return False

        if timezone.now() > self.activation_code_expiration:
            return False

        self.active = True
        self.verified = True
        self.save()
        return True


class UserProfile(BaseAccountModel):
    id_number = models.CharField(max_length=10, null=True)
    user = models.OneToOneField(User)
    phones = models.ManyToManyField('Phone', through='UserPhones')

    def get_bills(self):
        phones = self.phones.filter(active=True).all()
        response = {}
        for phone in phones:
            response[phone.number] = get_bill_info(phone.number)
        return response

    def reset_password(self, user):
        password = generate_password()
        user.set_password(password)
        user.save()
        return password

    def __unicode__(self):
        return self.user.username

    @property
    def owners(self):
        return self.user


class Phone(models.Model):
    number = models.CharField(max_length=11)

    def __unicode__(self):
        return self.number


class UserPhones(BaseAccountModel):
    user_profile = models.ForeignKey(UserProfile)
    phone = models.ForeignKey(Phone)

    class Meta:
        verbose_name_plural = "User Phones"


class MobileDevice(models.Model):
    imei = models.CharField(max_length=20, blank=True)
    push_token = models.CharField(max_length=512)
    user = models.ForeignKey(UserProfile)

    def __unicode__(self):
        return self.imei