import re
from xmlrpclib import ServerProxy

from django.conf import settings


### ADSL Services
def adsl_get_client():
    return ServerProxy(settings.API.get('CRM').get('URL'))

def adsl_get_auth(number):
    client = adsl_get_client()
    data = {'telNo': number, 'ip': '79.175.176.229'}
    response = client.WebServices.getAuthKey(data)
    data["key"] = response["key"]
    # data sample {'errcode': '0', 'errmsg': 'OK', 'key': '8a1022b12510695041b49779cee0c01f'}
    return data

def adsl_get_packages(number):
    data = adsl_get_auth(number)
    client = adsl_get_client()
    result = jsonify(client.WebServices.getPackageList(data))
    return result

def adsl_charge(number, package_id):
    data = adsl_get_auth(number)
    data["packId"] = package_id
    client = adsl_get_client()
    return client.WebServices.charge(data)

def adsl_get_status(number):
    data = adsl_get_auth(number)
    client = adsl_get_client()
    result = jsonify(client.WebServices.getCurrentStatus(data))
    return result

def adsl_purchase_list(number):
    data = adsl_get_auth(number)
    client = adsl_get_client()
    result = jsonify(client.WebServices.getPurchaseList(data))
    return result

def adsl_add_vas(number):
    data = adsl_get_auth(number)
    client = adsl_get_client()
    data["application"] = "music"
    data["item"] = "gold item"
    data["price"] = "10000"
    data["descr"] = "description"
    data["paidType"] = "1"
    data["dateTime"] = "1395/11/06"
    return client.WebServices.addVASPackage(data)

def jsonify(input_string):
    """
    making json response from CRM api call
    """
    output = {"errcode": input_string.get("errcode"), "errmsg": [{}]}
    msg = input_string["errmsg"].replace("\"", "").split(",")

    if input_string["errcode"] != "0":
        return input_string

    for item in msg:
        list_item = item.split(":")
        list_item = [re.sub(r"[^\w]", "", i) for i in list_item] #  Removing all characters

        if len(list_item) <= 1:
            output["errmsg"][0] = {}
            continue

        output["errmsg"][0][list_item[0]] = list_item[1] or []

    return output
