from django.conf.urls import url

from utility import views


urlpatterns = [
    url(r'^bills/(?P<username>[0-9]+)/$', view=views.BillList.as_view()),

    # exception and temporary
    url(r'^single-bill/$', view=views.get_bill_info_single_number),

    # ADSL
    url(r'^adsl/list/(?P<number>[0-9]+)/$', view=views.AdslList.as_view()),
    url(r'^adsl/status/(?P<number>[0-9]+)/$', view=views.AdslGetStatus.as_view()),
    url(r'^adsl/charge/$', view=views.AdslCharge.as_view()),
    url(r'^adsl/history/(?P<number>[0-9]+)/$', view=views.AdslPurchaseList.as_view()),
    url(r'^adsl/add/(?P<number>[0-9]+)/$', view=views.AddVAS.as_view()),
]
