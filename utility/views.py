from django.contrib.auth.models import User

from django.shortcuts import redirect, render
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import redirect, render
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from rest_framework import generics, viewsets, permissions, views
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.serializers import ValidationError
from rest_framework.decorators import api_view

from models import Utility
from serializers import BillListSerializer, AdslSeriliazer
from utils import adsl_get_packages, adsl_get_status, adsl_purchase_list, \
                    adsl_add_vas, adsl_charge
from accounts.models import Phone


class BillList(generics.ListAPIView):
    """
    Getting User's bills
    """
    serializer_class = BillListSerializer
    lookup_field = ('username')

    def get_queryset(self):
        return User.objects.get(username=self.kwargs['username']).userprofile

    def list(self, request, username):
        user_profile = self.get_queryset()
        result = user_profile.get_bills()
        return Response(result)


# ADSL Views
class AdslList(APIView):
    """
    Getting User's available packages or buy one
    """
    serializer_class = AdslSeriliazer
    permission_classes = ()

    def get(self, request, number):
        return Response(adsl_get_packages(number))


class AdslCharge(APIView):
    """
    Charging ADSL User account
    """
    serializer_class = AdslSeriliazer
    permission_classes = ()

    def post(self, request):
        number = request.data.get('number')
        package_id = request.data.get('package_id')        
        return Response(adsl_charge(number, package_id))


class AdslGetStatus(APIView):
    """
    Getting current status of ADSL user
    """
    serializer_class = AdslSeriliazer
    permission_classes = ()

    def get(self, request, number):
        return Response(adsl_get_status(number))


class AdslPurchaseList(APIView):
    """
    Getting list of purchased packages of ADSL user
    """
    serializer_class = AdslSeriliazer 
    permission_classes = ()

    def get(self, request, number):
        return Response(adsl_purchase_list(number))


class AddVAS(APIView):
    """
    Adding VAS Package to User
    """
    serializer_class = AdslSeriliazer
    permission_classes = ()

    def get(self, request, number):
        return Response(adsl_add_vas(number))


import urllib2
import base64
import json

def get_bill_info_internal_query(number):

    '''
    output = {}

    output['telNo'] = number
    output['billId'] = "11111111"
    output['payId'] = "22222222"
    output['amount'] = "33333333"
    output['status'] = "44444444"
    output['message'] = "55555555"
    return output
    '''

    url = 'https://Services.pec.ir/api/Telecom/Bill/GetBillInfo'

    username = 'aryan'
    password = '123123@'

    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (username,
password)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    request.add_header("Content-Type","application/json")
    data = json.dumps({'TelNo':number})
    result = urllib2.urlopen(request,data)

    j_response = json.loads(result.read().strip())


    output = {}

    output['telNo'] = number
    output['billId'] = j_response['BillID']
    output['payId'] = j_response['PayID']
    output['amount'] = j_response['Amount']
    output['status'] = j_response['Status']
    output['message'] = j_response['Message']
    return output

@api_view(['POST'])
def get_bill_info_single_number(request):

    if request.method=="POST":

        output = {}
        tel_no_string = request.data.get('tel_no')
        tel_no = long(tel_no_string)

        bill_info = get_bill_info_internal_query(tel_no)

        output['bill_info'] = bill_info
        response =  Response(output)
        return response
