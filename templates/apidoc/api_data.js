define({ "api": [
  {
    "group": "User",
    "type": "get",
    "url": "/accounts/user/:username",
    "title": "Getting a User",
    "description": "<p>Getting a User information</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "authenticated"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the User.</p>"
          }
        ]
      }
    },
    "filename": "tci_app/accounts/views.py",
    "groupTitle": "User",
    "name": "GetAccountsUserUsername",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"username\": \"09126330656\",\n  \"email\": \"info@magiavas.com\",\n  \"first_name\": \"Ahmad\",\n  \"last_name\": \"Rezaei\",\n  \"id_number\": \"04901524795\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "User",
    "type": "post",
    "url": "/accounts/user/activation/resend/",
    "title": "Resending User activation code",
    "description": "<p>Verifying the verification code User has received and returning JWT token</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the User.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"activation_code\": \"12345\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "tci_app/accounts/views.py",
    "groupTitle": "User",
    "name": "PostAccountsUserActivationResend"
  },
  {
    "group": "User",
    "type": "post",
    "url": "/accounts/user/activation/verify/",
    "title": "Verifying a User",
    "description": "<p>Verifying the verification code User has received and returning JWT token</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "activation_code",
            "description": "<p>Activation code User received</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"username\": \"09126330656\",\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "tci_app/accounts/views.py",
    "groupTitle": "User",
    "name": "PostAccountsUserActivationVerify"
  },
  {
    "group": "User",
    "type": "post",
    "url": "/accounts/users/",
    "title": "Creating a new User",
    "description": "<p>Creating new Users, if User exists but is not active, new activation code will be sent.</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "none"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"username\": \"09126330656\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "tci_app/accounts/views.py",
    "groupTitle": "User",
    "name": "PostAccountsUsers",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "first_name",
            "description": "<p>First name of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "last_name",
            "description": "<p>Last name of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id_number",
            "description": "<p>Personal ID number of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{ \n  \"user\": { \"username\": \"09124452123\"\n            \"password\": \"test\",\n            \"email\" : \"info@magiavas.com\",\n            \"first_name\": \"Ali\",\n            \"last_name\": \"Mohammadi\"\n          },\n  \"id_number\": \"0015248542\" \n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "group": "User",
    "type": "update",
    "url": "/accounts/user/:username",
    "title": "Updating a User",
    "description": "<p>Updating a User information</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "authenticated"
      }
    ],
    "filename": "tci_app/accounts/views.py",
    "groupTitle": "User",
    "name": "UpdateAccountsUserUsername",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"username\": \"09126330656\",\n  \"email\": \"info@magiavas.com\",\n  \"first_name\": \"Ahmad\",\n  \"last_name\": \"Rezaei\",\n  \"id_number\": \"04901524795\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "email",
            "description": "<p>Email of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "first_name",
            "description": "<p>First name of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "last_name",
            "description": "<p>Last name of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id_number",
            "description": "<p>Personal ID number of the User.</p>"
          }
        ]
      }
    }
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "tci_app/templates/apidoc/main.js",
    "group": "_home_amin_projects_tci_app_templates_apidoc_main_js",
    "groupTitle": "_home_amin_projects_tci_app_templates_apidoc_main_js",
    "name": ""
  }
] });
