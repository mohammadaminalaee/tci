#!/usr/bin/env python
# -*- coding: utf-8 -*-

from suds.client import Client
from celery import shared_task

from django.conf import settings

@shared_task
def send_activation_sms(recipient, activation_code):
    url = settings.API.get('SMS').get('URL')
    username = settings.API.get('SMS').get('USERNAME')
    password = settings.API.get('SMS').get('PASSWORD')
    src = settings.API.get('SMS').get('SRC')

    data = {}
    data['userCredential'] = {'username': username, 'password': password}
    data['srcAddresses'] = src
    data['destAddresses'] = recipient
    data['msgBody'] = u'به مخابرات شگفت انگیز خوش آمدید. کد فعال سازی شما:‌ {}'.format(activation_code)
    data['msgEncoding'] = ''

    client = Client(url)
    client.service.sendSms(**data)


@shared_task
def send_new_password(recipient, new_password):
    url = settings.API.get('SMS').get('URL')
    username = settings.API.get('SMS').get('USERNAME')
    password = settings.API.get('SMS').get('PASSWORD')
    src = settings.API.get('SMS').get('SRC')

    data = {}
    data['userCredential'] = {'username': username, 'password': password}
    data['srcAddresses'] = src
    data['destAddresses'] = recipient
    data['msgBody'] = u'رمز عبور جدید شما در مخابرات شگفت انگیز:‌ {}'.format(new_password)
    data['msgEncoding'] = ''

    client = Client(url)
    client.service.sendSms(**data)


@shared_task
def send_activation_ivr(recipient, activation_code):
    url = settings.API.get('IVR').get('URL')
    client = Client(url)
    src = 'w2p' + activation_code
    client.service.Originate(src, recipient)
