from django.contrib import admin

from models import UserProfile, Phone, MobileDevice, UserPhones


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'active', 'verified', 'id_number']
    readonly_fields = ('activation_code', 'activation_code_expiration', 'registration_date')
    search_fields = ['user']


class UserPhonesAdmin(admin.ModelAdmin):
    list_display = ['user_profile', 'phone', 'active', 'verified']
    readonly_fields = ('activation_code', 'activation_code_expiration', 'registration_date')


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Phone)
admin.site.register(UserPhones, UserPhonesAdmin)
admin.site.register(MobileDevice)

admin.site.site_header = 'TCI SAMSSON SDP Administration'
admin.site.site_title = 'TCI SAMSSON SDP Adminstration'
