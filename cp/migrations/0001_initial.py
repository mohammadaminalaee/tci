# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-27 11:51
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('aggregator', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CP',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('en_name', models.CharField(max_length=64)),
                ('fa_name', models.CharField(max_length=64)),
                ('id_number', models.CharField(max_length=12)),
                ('id_image', models.ImageField(upload_to=b'')),
                ('phone', models.CharField(max_length=12)),
                ('postal_code', models.CharField(max_length=10)),
                ('address', models.TextField()),
                ('licence_image', models.ImageField(blank=True, upload_to=b'')),
                ('aggregator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='aggregator.Aggregator')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
