from django.utils import timezone

from rest_framework import generics
from rest_framework.views import APIView

from models import App, ApplicationAsset, Campaign, AppRating, Category
from serializers import AppSerializer, AppAssetSerializer, CampaignSerializer, \
                        AppRatingSerializer, CategorySerializer
from payment.models import PaymentType
from payment.serializers import PaymentTypeSerializer



class AppList(generics.ListCreateAPIView):
    serializer_class = AppSerializer
    queryset = App.objects.all()
    lookup_field = ('id',)
    permission_classes = ()


class AppDetail(generics.RetrieveAPIView):
    serializer_class = AppSerializer
    queryset = App.objects.all()
    permission_classes = ()


class AppRatingDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AppRatingSerializer
    queryset = AppRating.objects.all()


class AppRatingList(generics.ListCreateAPIView):
    serializer_class = AppRatingSerializer
    queryset = AppRating.objects.all()
    permission_classes = ()


class AppAssetList(generics.ListCreateAPIView):
    serializer_class = AppAssetSerializer
    queryset = ApplicationAsset.objects.all()


class AppAssetDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AppAssetSerializer
    queryset = ApplicationAsset.objects.all()


class CampaignList(generics.ListAPIView):
    serializer_class = CampaignSerializer
    queryset = Campaign.objects.all()
    permission_classes = ()

    # def get_queryset(self):
    #     """
    #     This view should return a list of all the running campaigns
    #     """
    #     print self.request.data
    #     start = self.request.start_date
    #     end = self.request.end_date
    #     now = timezone.now()
    #     return Campaign.objects.filter(start < now).filter(end > now)


class CategoryList(generics.ListCreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_field = 'en_name'


class PaymentTypeList(generics.ListCreateAPIView):
    serializer_class = PaymentTypeSerializer
    queryset = PaymentType.objects.all()


class PaymentTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PaymentTypeSerializer
    queryset = PaymentType.objects.all()
    lookup_field = 'en_name'