from django.contrib.auth.models import User
from django.db import models



class Aggregator(models.Model):
    en_name = models.CharField(max_length=64)
    fa_name = models.CharField(max_length=64)
    company_registration_code = models.CharField(max_length=32)
    company_financial_code = models.CharField(max_length=32)
    ceo_name = models.CharField(max_length=16)
    ceo_phone = models.CharField(max_length=12)
    support_name = models.CharField(max_length=16)
    support_phone = models.CharField(max_length=12) 
    support_email = models.EmailField()
    accountant_name = models.CharField(max_length=16)
    accountant_phone = models.CharField(max_length=12)
    vas_manager_name = models.CharField(max_length=16)
    vas_manager_phone = models.CharField(max_length=12)
    address = models.TextField()

    user = models.OneToOneField(User)

    def __unicode__(self):
        return self.user.username

    @property
    def owners(self):
        return [self.user]

    @property
    def user__username(self):
        return self.user.username
