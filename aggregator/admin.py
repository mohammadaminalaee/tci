from django.contrib import admin

from models import Aggregator

class AggregatorAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name', 'user']


admin.site.register(Aggregator, AggregatorAdmin)