from django.db import models
from django.contrib.auth.models import User

from utility.models import Utility
from application.models import AppItem


class PaymentType(models.Model):
    en_name = models.CharField(max_length=16)
    fa_name = models.CharField(max_length=16)

    def __unicode__(self):
        return self.en_name


class BaseTransaction(models.Model):
    amount = models.DecimalField(max_digits=11, decimal_places=4)
    status = models.BooleanField(default=False)
    rrn = models.CharField(max_length=32, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    au = models.CharField(max_length=16, blank=True)

    payment_type = models.ForeignKey(PaymentType)
    user = models.ForeignKey(User)

    class Meta:
        abstract = True


class UtilTransaction(BaseTransaction):
    util = models.ForeignKey(Utility)
    pay_id = models.CharField(max_length=32, default=0)
    bill_id = models.CharField(max_length=32, default=0)


class AppTransaction(BaseTransaction):
    app_item = models.ForeignKey(AppItem)