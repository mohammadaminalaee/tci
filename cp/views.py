from django.contrib.auth.models import User

from rest_framework import generics

from models import CP
from serializers import CpSerializer
from accounts.permissions import IsOwner

class CpList(generics.ListCreateAPIView):
    """
    Getting all CPs or creating new one
    """
    queryset = CP.objects.all()
    serializer_class = CpSerializer

    def perform_create(self, serializer):
        user_data = serializer.validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        cp = CP.objects.create(user=user, **serializer.validated_data)
        serializer.validated_data['user'] = user_data
        return cp


class CpDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Get/Update/Delete CP by username
    """
    serializer_class = CpSerializer
    permission_classes = (IsOwner,)
    queryset = CP.objects.all()
    lookup_field = 'user__username'

