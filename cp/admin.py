from django.contrib import admin

from models import CP


class CPAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name','user']


admin.site.register(CP, CPAdmin)
