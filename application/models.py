from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from django.db import models



class Category(models.Model):
    en_name = models.CharField(max_length=32)
    fa_name = models.CharField(max_length=32)

    class Mete:
        verbose_name_plural = "Categories"

    def __unicode__(self):
        return self.en_name


class App(models.Model):
    en_name = models.CharField(max_length=32)
    fa_name = models.CharField(max_length=32)
    api_username = models.CharField(max_length=32)
    api_password = models.CharField(max_length=128)
    build_number = models.CharField(max_length=32)
    owner_email = models.EmailField()
    owner_contact = models.CharField(max_length=32)
    active = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)
    purchase_callback = models.URLField()

    assets = models.ManyToManyField('ApplicationAsset')
    payment_types = models.ManyToManyField('payment.PaymentType')
    aggregator = models.ForeignKey('aggregator.Aggregator')
    cp = models.ForeignKey('cp.CP')
    category = models.ForeignKey(Category)

    @property
    def is_active(self):
        return self.active and self.verified

    def in_app_items(self):
        return self.app_items.count()

    def __unicode__(self):
        return self.en_name


class AppItem(models.Model):
    en_name = models.CharField(max_length=32)
    fa_name = models.CharField(max_length=32)
    active = models.BooleanField(default=False)
    on_sale = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=11, decimal_places=4)

    appp_item_instance = models.CharField(max_length=128)

    app = models.ForeignKey(App, related_name='app_items')
    payment_types = models.ManyToManyField('payment.PaymentType')

    class Meta:
        verbose_name_plural = "App Items"

    @property
    def is_active(self):
        return self.active

    def __unicode__(self):
        return self.en_name


    def make_app_transaction(self, username, au=12345):
        from payment.models import AppTransaction
        user = User.objects.get(username=username)
        payment_type = self.payment_types.first() # online only
        transaction = AppTransaction.objects.create(app_item=self, amount=self.price,
                                    payment_type=payment_type, user=user, au=au)
        return transaction


class ApplicationAsset(models.Model):
    image = models.ImageField()

    def preview(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.image))

    preview.short_description = 'Image'


class Campaign(models.Model):
    title = models.CharField(max_length=32)
    description = models.CharField(max_length=256)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    image = models.ImageField()
    thumbnail = models.ImageField()

    app = models.ForeignKey(App)


class AppRating(models.Model):
    app = models.ForeignKey(App, related_name='ratings', on_delete=models.CASCADE)
    rating = models.IntegerField()
    comment = models.CharField(max_length=45)
