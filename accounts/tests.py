import json

from faker import Faker

from django.test import TestCase
from django.contrib.auth.models import User

from models import UserProfile, MobileDevice, Phone

fake = Faker()


# Testing Views

class UserRegistrationCase(TestCase):

    def test_registration(self):
        data = {
                'username': fake.username()[:10],
                'password': fake.email(),
                'id_number': fake.phonenumber()[:8]
                }
        response = self.client.post('/accounts/users/', data)
        self.assertEqual(response.status_code, 201)


class UserViewTestCase(TestCase):
    
    data = {}

    def setUp(self):
        username = fake.phonenumber()[:10].replace('-', '')
        password = fake.email()
        id_number = fake.phonenumber()[:8]
        user = User.objects.create_user(username=username, password=password)
        self.data['user_profile'] = UserProfile.objects.create(user=user, id_number=id_number)
        data = {'username': username, 'password': password}
        token = self.client.post('/accounts/api-token-auth/', data).data.get('token')
        self.data['auth'] = 'JWT ' + token

    def test_user_activation(self):
        user_profile = self.data['user_profile']
        activation_data = {
            'username': user_profile.user.username,
            'activation_code': user_profile.activation_code}

        response = self.client.post('/accounts/user/activation/verify/', activation_data)
        self.assertEqual(response.status_code, 201)

    def test_resend_activation(self):
        user = self.data['user_profile'].user
        data = json.dumps({'username': user.username})
        response = self.client.put('/accounts/user/activation/resend/', data, content_type="application/json")
        self.assertEqual(response.status_code, 200)

    # def test_get_user(self):
    #     user = self.data['user_profile'].user
    #     url = '/accounts/user/' + user.username
    #     response = self.client.get(url, HTTP_AUTHORIZATION=self.data['auth'])
    #     self.assertEqual(response.status_code, 200)


# class PhoneViewTestCase(TestCase):

#     data = {}

#     def setUp(self):
#         password = generate_random()
#         user = User.objects.create_user(username=generate_random(), password=password)
#         user_profile = UserProfile.objects.create(user=user)
#         data = {'username': user.username, 'password': password}
#         response = self.client.post('/accounts/api-token-auth/', data)
#         self.data['token'] = response.data.get('token')
#         self.assertEqual(response.status_code, 200)


# Testing Models
class UserModelTestCase(TestCase):

    data = {}

    def setUp(self):
        username = fake.username()[:10].replace('-', '')
        password = fake.email()
        id_number = fake.phonenumber()[:8]
        number = fake.phonenumber()
        user = User.objects.create_user(username=username, password=password)
        self.data['user_profile'] = UserProfile.objects.create(user=user, id_number=id_number)


    def test_registration(self):
        user_profile = self.data['user_profile']
        user = user_profile.user

        queried_user = User.objects.get(username=user.username)
        self.assertEqual(user, queried_user)
        self.assertEqual(user_profile, queried_user.userprofile)

    def test_activation(self):
        user_profile = self.data['user_profile']
        self.assertTrue(user_profile.verify_activation_code(user_profile.activation_code))

    def test_regenerate_token(self):
        user_profile = self.data['user_profile']
        code = user_profile.generate_activation_code()
        self.assertTrue(user_profile.verify_activation_code(code))