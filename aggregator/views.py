from django.contrib.auth.models import User

from rest_framework import generics

from models import Aggregator
from serializers import AggregatorSerializer
from accounts.permissions import IsOwner


class AggregatorList(generics.ListCreateAPIView):
    """
    Getting all Aggregators or creating new one
    """
    queryset = Aggregator.objects.all()
    serializer_class = AggregatorSerializer

    def perform_create(self, serializer):
        user_data = serializer.validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        aggregator = Aggregator.objects.create(user=user, **serializer.validated_data)
        serializer.validated_data['user'] = user_data
        return aggregator


class AggregatorDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Get/Update/Delete Aggregators by username
    """
    serializer_class = AggregatorSerializer
    queryset = Aggregator.objects.all()
    permission_classes = (IsOwner,)
    lookup_field = 'user__username'

