from django.contrib.auth.models import User

from rest_framework import serializers

from aggregator.models import Aggregator


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'password', 'email')
        extra_kwargs = {
            'username': {'validators': []},
        }



class AggregatorSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Aggregator
        fields = (
                    'user',
                    'en_name',
                    'fa_name',
                    'company_registration_code',
                    'company_financial_code',
                    'ceo_name',
                    'ceo_phone',
                    'support_name',
                    'support_phone',
                    'support_email',
                    'accountant_name',
                    'accountant_phone',
                    'vas_manager_name',
                    'vas_manager_phone',
                    'address')

