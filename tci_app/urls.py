# TODO DEBUG Only
from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^docs/', include('rest_framework_docs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^application/', include('application.urls')),
    url(r'^utility/', include('utility.urls')),
    url(r'^payment/', include('payment.urls')),
    url(r'^aggregator/', include('aggregator.urls')),
    url(r'^cp/', include('cp.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
