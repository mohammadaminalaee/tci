from django.conf.urls import url

from application import views


urlpatterns = [

    # Application
    url(r'^apps/$', view=views.AppList.as_view()),
    url(r'^app/(?P<pk>[0-9]+)/$', view=views.AppDetail.as_view(), name='app-detail'),

    url(r'^ratings/$', view=views.AppRatingList.as_view()),
    url(r'^rating/(?P<pk>)/$', view=views.AppRatingDetail.as_view()),

    url(r'^assets/$', view=views.AppAssetList.as_view()),
    url(r'^asset/(?P<pk>[0-9]+)/$', view=views.AppAssetDetail.as_view(), name='applicationasset-detail'),

    url(r'^campaigns/$', view=views.CampaignList.as_view()),

    url(r'^categories/$', view=views.CategoryList.as_view()),
    url(r'^category/(?P<en_name>[\w]+)/$', view=views.CategoryDetail.as_view()),

    url(r'^payment-types/$', view=views.PaymentTypeList.as_view()),
    url(r'^payment-type/(?P<en_name>[\w]+)/$', view=views.PaymentTypeDetail.as_view()),
]
