from django.conf.urls import url

from aggregator import views

urlpatterns = [
    url(r'^users/$', view=views.AggregatorList.as_view()),
    url(r'^user/(?P<user__username>[\w]+)/$', view=views.AggregatorDetail.as_view()),
]

