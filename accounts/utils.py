import datetime
from random import randint


from django.utils import timezone


def jwt_response_payload_handler(token, user=None, request=None):
    phones = user.userprofile.phones
    bills = user.userprofile.get_bills()
    return {
        'token': token,
        'username': user.username,
        'email': user.email,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'active': user.userprofile.is_active,
        'phones': phones.values(),
        'bills': bills,
    }

def generate_jwt_token(user):
    from rest_framework_jwt.settings import api_settings
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token

def generate_activation_code():
    """
    Generating a four digit long activation code
    """
    return str(randint(999, 9999))

def generate_activation_time():
    """
    Generating a future time for activation code
    """
    return timezone.now() + datetime.timedelta(minutes=10)

def generate_password():
    """
    Generating new password
    """
    return str(randint(9999999, 99999999))