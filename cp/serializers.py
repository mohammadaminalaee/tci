from django.contrib.auth.models import User

from rest_framework import serializers

from aggregator.serializers import AggregatorSerializer
from aggregator.models import Aggregator
from models import CP


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'password', 'email')
        extra_kwargs = {
            'username': {'validators': []},
        }



class CpSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    promoting_aggregator = serializers.SlugRelatedField(
                            slug_field='user__username',
                            queryset=Aggregator.objects.all())


    class Meta:
        model = CP
        fields = (
                    'user',
                    'promoting_aggregator',
                    'en_name',
                    'fa_name',
                    'id_number',
                    'id_image',
                    'phone',
                    'postal_code',
                    'address',
                    'license_image')
