from django.contrib.auth.models import User
from django.core import serializers


from rest_framework.serializers import ValidationError
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from permissions import IsOwner
from utils import generate_jwt_token
from models import UserProfile, Phone, UserPhones
from tasks import send_activation_sms, send_new_password, send_activation_ivr
from serializers import UserProfileSerializer, UserSerializer, PhoneSerializer,\
                        UserPhoneSerializer, PhoneActivationSerializer


class UserList(APIView):
    """
    @apiDefine UserParams
    @apiParam {Number} username  Username of the User.
    @apiParam {String} password  Password of the User.
    @apiParam {String} [email]  Email of the User.
    @apiParam {String} [first_name]  First name of the User.
    @apiParam {String} [last_name]  Last name of the User.
    @apiParam {Number} [id_number] Personal ID number of the User.
    """
    """
    @apiDefine UserSuccess
    @apiSuccessExample {json} Success-Response:
      HTTP/1.1 200 OK
      {
        "username": "09126330656",
        "email": "info@magiavas.com",
        "first_name": "Ahmad",
        "last_name": "Rezaei",
        "id_number": "04901524795"
      }
    """
    """
    @apiDefine UseParamsExample
    @apiParamExample {json} Request-Example:
      { 
        "user": { "username": "09124452123"
                  "password": "test",
                  "email" : "info@magiavas.com",
                  "first_name": "Ali",
                  "last_name": "Mohammadi"
                },
        "id_number": "0015248542" 
      }
    """

    """
    @apiGroup User
    @api {post} /accounts/users/ Creating a new User
    @apiDescription Creating new Users, if User exists but is not active, new activation code will be sent.
    @apiVersion 1.0.0
    @apiPermission none
    @apiUse UserParams
    @apiUse UseParamsExample
    
    @apiSuccessExample {json} Success-Response:
      HTTP/1.1 200 OK
      {
        "username": "09126330656"
      }
    """
    permission_classes = ()

    def post(self, request):
        user_data = request.data.pop('user')

        try:
            user = User.objects.get(username=user_data['username'])
            user_profile = user.userprofile
            if user_profile.is_active:
                return Response({"username": "User with this username already exists"}, status=status.HTTP_409_CONFLICT)

            activation_code = user_profile.generate_activation_code()
            send_activation_sms.delay(recipient=user.username, activation_code=activation_code)
            return Response({"user": "User acttivation code was resent"})

        except User.DoesNotExist:
            # if not user, should be created
            user = User.objects.create_user(**user_data)
            user_profile = UserProfile.objects.create(user=user, **request.data)
            send_activation_sms.delay(recipient=user.username, activation_code=user_profile.activation_code)
            return Response({"username": user.username})      


class UserDetail(generics.RetrieveUpdateAPIView):
    """
    @apiGroup User
    @api {get} /accounts/user/:username Getting a User
    @apiDescription Getting a User information
    @apiVersion 1.0.0
    @apiPermission authenticated
    @apiUse UserSuccess

    @apiParam {Number} username  Username of the User.
    """
    """
    @apiGroup User
    @api {update} /accounts/user/:username Updating a User
    @apiDescription Updating a User information
    @apiVersion 1.0.0
    @apiPermission authenticated
    @apiUse UserSuccess
    @apiUse UserParams
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    lookup_field = 'username'

    def perform_update(self, serializer):
        user = serializer.save()
        user.set_password(self.request.data.get('password'))
        user.save()


class UserActivation(APIView):
    """
    @apiGroup User
    @api {post} /accounts/user/activation/verify/ Verifying a User
    @apiDescription Verifying the verification code User has received and returning JWT token
    @apiVersion 1.0.0
    @apiPermission none

    @apiParam {Number} username  Username of the User.
    @apiParam {Number} activation_code Activation code User received

    @apiSuccessExample {json} Success-Response:
      HTTP/1.1 200 OK
      {
        "username": "09126330656",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9"
      }
    """
    permission_classes = ()

    def post(self, request):
        activation_code = request.data.get('activation_code')
        user = User.objects.filter(username=request.data.get('username')).first()

        if not user:
            raise ValidationError('User does not exist')

        if not user.userprofile.verify_activation_code(activation_code):
            raise ValidationError('Invalid activation code')

        token = generate_jwt_token(user)
        return Response({"username": user.username, "token": token})


class UserResendActivation(APIView):
    """
    @apiGroup User
    @api {post} /accounts/user/activation/resend/ Resending User activation code
    @apiDescription Verifying the verification code User has received and returning JWT token
    @apiVersion 1.0.0
    @apiPermission none

    @apiParam {Number} username  Username of the User.

    @apiSuccessExample {json} Success-Response:
      HTTP/1.1 200 OK
      {
        "activation_code": "12345",
      }
    """
    permission_classes = ()

    def post(self, request):
        username = request.data.get('username')
        user_profile = User.objects.get(username=username).userprofile
        activation_code = user_profile.generate_activation_code()
        send_activation_sms.delay(recipient=username, activation_code=activation_code)
        return Response('activation_code: {}'.format(activation_code))




class UserPhoneCreate(generics.CreateAPIView):
    """
    Adding Phones for User
    """
    serializer_class = PhoneSerializer
    queryset = UserPhones.objects.all()

    def perform_create(self, serializer):
        username = serializer.data.get('username')
        user = User.objects.filter(username=username).first()

        if not user:
            raise ValidationError('User not found')

        phone, created = Phone.objects.get_or_create(number=serializer.data.get('number'))
        duplicate_user_phone = UserPhones.objects.filter(user_profile=user.userprofile, phone=phone)

        if duplicate_user_phone:
            raise ValidationError('User already follows this phone')

        user_phone = UserPhones.objects.create(user_profile=user.userprofile, phone=phone)
        send_activation_ivr.delay(recipient=phone.number, activation_code=user_phone.activation_code)
        return Response(serializer.data)


class UserPhonesList(generics.ListAPIView):
    """
    Getting User's Phones with only username
    """
    serializer_class = UserPhoneSerializer
    queryset = UserPhones.objects.all()
    lookup_field = 'username'

    def get_queryset(self):
        return UserPhones.objects.filter(user_profile__user__username=self.kwargs['username'])

        
class UserPhonesDetail(generics.DestroyAPIView):
    """
    Deleting User's Phones with username, number
    """
    serializer_class = UserPhoneSerializer
    queryset = UserPhones.objects.all()
    lookup_field = ('username', 'number')

    def get_object(self):
        number = self.kwargs.get('number')
        user_profile = User.objects.get(username=self.kwargs.get('username')).userprofile
        return UserPhones.objects.filter(user_profile=user_profile, phone__number=number).first()

    def perform_destroy(self, user_phone):
        if not user_phone:
            raise ValidationError('Phone number does not exist')

        user_phone.delete()


class PhoneActivation(APIView):
    """
    Verifying phone activation
    """
    def post(self, request):
        activation_code = request.data.get('activation_code')
        user = User.objects.filter(username=request.data.get('username')).first()
        phone = Phone.objects.filter(number=request.data.get('number')).first()
        user_phone = UserPhones.objects.get(user_profile=user.userprofile, phone=phone)

        if not user:
            raise ValidationError('User does not exist')

        if not phone:
            raise ValidationError('Phone does not exist')

        if not user_phone.verify_activation_code(activation_code):
            raise ValidationError('Invalid activation code')

        return Response('Success')


class PhoneResendActivation(APIView):
    """
    Resending phone activation code
    """
    def post(self, request):
        username = request.data.get('username')
        number = request.data.get('number')
        user_phone = UserPhones.objects.get(user_profile__user__username=username, phone__number=number)
        activation_code = user_phone.generate_activation_code()
        send_activation_ivr.delay(recipient=number, activation_code=activation_code)
        return Response('New code: {}'.format(activation_code))


class ChangePassword(APIView):
    """
    PUT: reset password with username
    POST: change password (username, password, new_password)
    """
    permission_classes = ()

    def put(self, request, username):
        user = User.objects.get(username=username)
        password = user.userprofile.reset_password(user)
        send_new_password(recipient=username, new_password=password)
        return Response('New password: {}'.format(password))

    def post(self, request):
        password = request.data.get('password')
        new_password = request.data.get('new_password')

        user = User.objects.get(username=request.data.get('username'))

        if not user.check_password(password):
            raise ValidationError('Wrong password')

        user.set_password(new_password)
        user.save()
        return Response('New password: {}'.format(new_password))

