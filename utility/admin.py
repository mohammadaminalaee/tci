from django.contrib import admin

from models import Utility


class UtilityAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name']


admin.site.register(Utility, UtilityAdmin)
