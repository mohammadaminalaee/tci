from rest_framework import permissions
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


jwt_auth = JSONWebTokenAuthentication()


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners to view/edit CP
    """

    def has_object_permission(self, request, view, obj):
        # if not jwt token available
        if not request.META.get('HTTP_AUTHORIZATION'):
           return False

        user, token = jwt_auth.authenticate(request)

        if user.is_superuser:
            return True

        return user in obj.owners