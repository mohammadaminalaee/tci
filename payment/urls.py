from django.conf.urls import url

from payment import views

urlpatterns = [
    # IPG  Bill Payment
    url(r'^bill/start/$', view=views.ipg_bill_payment),
    url(r'^bill/verify/$', view=views.bill_verification),
    url(r'^ipg/verify/$', view=views.ipg_verification),

    # IPG ADSL Payment
    url(r'^adsl/start/$', view=views.ipg_adsl_payment),

    # Two Factor Bill Payment
    url(r'^quickpay/$', view=views.TwoFactorPayment.as_view()),

    url(r'^in-app/notification/$', view=views.in_app_purchase),
]
