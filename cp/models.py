from django.contrib.auth.models import User
from django.db import models

from aggregator.models import Aggregator


class CP(models.Model):
    en_name = models.CharField(max_length=64)
    fa_name = models.CharField(max_length=64)
    id_number = models.CharField(max_length=12)
    id_image = models.ImageField()
    phone = models.CharField(max_length=12)
    postal_code = models.CharField(max_length=10)
    address = models.TextField()
    license_image = models.ImageField(blank=True)


    user = models.OneToOneField(User)
    promoting_aggregator = models.ForeignKey(Aggregator)

    def __unicode__(self):
        return self.user.username

    @property
    def owners(self):
        return [self.user, self.promoting_aggregator.user]
