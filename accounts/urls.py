from django.conf.urls import url

from rest_framework_jwt.views import obtain_jwt_token

from accounts import views

urlpatterns = [
    # User Related URLS
    url(r'^users/$', view=views.UserList.as_view()),
    url(r'^user/(?P<username>[0-9]+)/$', view=views.UserDetail.as_view()),
    url(r'^user/activation/verify/$', view=views.UserActivation.as_view()),
    url(r'^user/activation/resend/$', view=views.UserResendActivation.as_view()),
    url(r'^user/password/(?P<username>[0-9]+)/$', view=views.ChangePassword.as_view()),
    url(r'^user/password/$', view=views.ChangePassword.as_view()),

    # # Phone related URLs
    url(r'^phones/(?P<username>[0-9]+)/$', view=views.UserPhonesList.as_view()),
    url(r'^phones/(?P<username>[0-9]+)/(?P<number>[0-9]+)/$', view=views.UserPhonesDetail.as_view()),
    url(r'^phones/create/$', view=views.UserPhoneCreate.as_view()),
    url(r'^phone/activation/verify/$', view=views.PhoneActivation.as_view()),
    url(r'^phone/activation/resend/$', view=views.PhoneResendActivation.as_view()),

    url(r'^api-token-auth/', view=obtain_jwt_token),
]

