from django.contrib import admin

from models import App, AppItem, Category, ApplicationAsset, Campaign, AppRating


class AppAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name', 'active', 'verified', 'in_app_items']
    list_filter = ('active', 'verified')
    search_fields = ['en_name', 'fa_name']
    list_per_page = 50


class AppItemAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name', 'app', 'active', 'on_sale', 'price']
    list_filter = ('active', 'on_sale')
    search_fields = ['en_name', 'fa_name']
    list_per_page = 50


class AppilcationAssetAdmin(admin.ModelAdmin):
    list_display = ['preview']
    list_per_page = 50


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name']
    search_fields = ['en_name', 'fa_name']


admin.site.register(App, AppAdmin)
admin.site.register(AppItem, AppItemAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ApplicationAsset, AppilcationAssetAdmin)
admin.site.register(Campaign)
admin.site.register(AppRating)
