from datetime import datetime
from time import mktime
import requests
import base64
import json
import re

from django.conf import settings

from suds.client import Client

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_public_key
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric.padding import PKCS1v15

import logging

logger = logging.getLogger(__name__)

class SafeString(str):
    def title(self):
        return self

    def capitalize(self):
        return self


def get_bill_info(phone_number):
    """
    Getting bill information

    Args:
        phone_number (str) : phone number to be queried
        
    Returns:
        {
            "TelNo": phone number,
            "BillID": bill ID,
            "PayID": payment ID,
            "Amount": amount,
            "Status": response status,
            "Message": message 
        }
    """
    username = settings.API.get("getBillInfo").get("USERNAME")
    password = settings.API.get("getBillInfo").get("PASSWORD")
    url = settings.API.get("getBillInfo").get("URL")

    headers = generate_pec_headers(username, password)
    result = requests.post(url, json={'TelNo': phone_number}, headers=headers).json()
    return result

def pay_bill(mobile_number, bill_id, pay_id, pan, pin2):
    """
    Paying Bill with two factor payment
        
    Returns:
        {
            "TelNo": phone number,
            "BillID": bill ID,
            "PayID": payment ID,
            "Amount": amount,
            "Status": response status,
            "Message": message 
        }
    """
    pec_request = {}
    pec_request['MobileNo'] = mobile_number
    pec_request['PayInfo'] = generate_pay_info(pan, pin2)
    pec_request['Token'] = '0'
    pec_request['BillId'] = bill_id
    pec_request['PayId'] = pay_id
    pec_request['TerminalPin'] = settings.API.get("pec_terminal_pin")

    username = settings.API.get("billPaymentGeneral").get("USERNAME")
    password = settings.API.get("billPaymentGeneral").get("PASSWORD")
    url = settings.API.get("billPaymentGeneral").get("URL")

    headers = generate_pec_headers(username, password)
    headers["appVersion"] = "1.7"

    result = requests.post(url, json=pec_request, headers=headers).json()

    if result["Status"] == 0:
        return payment_confirmation(bill_id, pay_id, result["Data"]["TraceNo"])

    return result

def payment_confirmation(bill_id, pay_id, rrn):

    username = settings.API.get("setPayInfo").get("USERNAME")
    password = settings.API.get("setPayInfo").get("PASSWORD")
    url = settings.API.get("setPayInfo").get("URL")

    headers = generate_pec_headers(username, password)

    data = {}
    data['BillId'] = bill_id
    data['PayId'] = pay_id
    data['RRN'] = rrn

    result = requests.post(url, json=data, headers=headers).json()
    return result

def generate_pay_info(pan, pin2):

    message = {}
    message['PAN'] = pan
    message['Pin2'] = pin2
    message['ExpY'] = "0000"
    message['ExpM'] = "0000"
    message['CV'] = "0000"

    j_message = json.dumps(message)
    public_key_file= open(settings.KEY_ROOT + "/" + "pec_public_key.pem").read()
    public_key = serialization.load_pem_public_key(public_key_file, default_backend())
    ciphertext = public_key.encrypt(j_message, PKCS1v15())
    ciphertext_base_64 = base64.encodestring(ciphertext)
    return ciphertext_base_64.rstrip()

def generate_pec_headers(username, password):
    b64_string = base64.encodestring('{0}:{1}'.format(username, password)).replace('\n', '')
    headers = {"Content-Type": "application/json", "Authorization": "Basic {0}".format(b64_string)}
    return headers


def make_ipg_payment(amount):
    url = settings.API.get('IPG_Payment').get('URL')
    terminal_pin = settings.API.get('IPG_Payment').get('PIN')
    callback_url = settings.API.get('IPG_Payment').get('Callback')
    auth_url = settings.API.get('IPG_Payment').get('Auth')
    client = Client(url)

    data = {}
    data['orderId'] = int(mktime(datetime.now().timetuple()))
    data['amount'] = amount
    data['authority'] = 0
    data['status'] = 1
    data['callbackUrl'] = callback_url
    data['pin'] = terminal_pin

    auth_response = client.service.PinPaymentRequest(**data)
    authority_url = auth_url + str(auth_response['authority'])
    return auth_response['authority'], authority_url


def verify_ipg_payment(authority, status):
    url = settings.API.get('IPG_Payment').get('URL')
    terminal_pin = settings.API.get('IPG_Payment').get('PIN')
    client = Client(url)

    data = {}
    data['pin'] = terminal_pin
    data['status'] = status
    data['authority'] = authority

    response = client.service.PinPaymentEnquiry(**data)

    if response['status'] == 0:
        return True
    return False

def verify_bill_payment(authority, status):
    url = settings.API.get('billPayment').get('URL')
    terminal_pin = settings.API.get('billPayment').get('PIN')
    client = Client(url)

    data = {}
    data['pin'] = terminal_pin
    data['status'] = status
    data['authority'] = authority

    response = client.service.PinBillPaymentEnquiry(**data)

    if response['status'] == 0:
        return True
    return False
 
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
import base64


def generate_public_private_pair():

    # generate private key
    private_key = rsa.generate_private_key(public_exponent=65537,key_size=2048,backend=default_backend())
    pem = private_key.private_bytes(encoding=serialization.Encoding.PEM,format=serialization.PrivateFormat.TraditionalOpenSSL,encryption_algorithm=serialization.NoEncryption())
    lines = pem.splitlines()
    private_key_file = open(settings.KEY_ROOT + "/" + "client_private_key.pem", "w")
    for line in lines:
        private_key_file.write(line)
        private_key_file.write("\n")
    private_key_file.close()

    # generate public key
    public_key = private_key.public_key()
    pem = public_key.public_bytes(encoding=serialization.Encoding.PEM,format=serialization.PublicFormat.SubjectPublicKeyInfo)
    lines = pem.splitlines()
    public_key_file = open(settings.KEY_ROOT + "/" +  "client_public_key.pem", "w")
    for line in lines:
        public_key_file.write(line)
        public_key_file.write("\n")
    public_key_file.close()

def decrypt_pin_pan(cipher_text):
    #cipher_text = "NJVx+CyzLiotD2zmOvASirKpuiXzdGte3n6/lY6DNGCYGqBqEJOO2ayGyduqHUubF23poxNpTFT+wNhhiJ5nghwNux1/0jf65K3fnnEdi8dE6RV13WIHcRZa1pT5sgHULy73fS8Gz3HGLkaKc/DJ+Cz0/eN9D1nYLLP+vVZEZyqeHFlbDIuiTsf6RbGOClassZWCth4Lv+kEW8aexXJOSRqyo/c+EK66Cqp16upLwI5Qu/039bK/xLWCcBiOTTyqZVUuMXK5PkCRqsNlzoCRhobOLgh/6ty6Xwi1aEjDTeP/CTV9yCHZwx2KQrjel2zXgLFYM777Uzr+oO4ikdS1TA=="
    ciphertext_byte = base64.b64decode(cipher_text)

    with open(settings.KEY_ROOT + "/" + "client_private_key.pem", "rb") as private_key_file:
         private_key = serialization.load_pem_private_key(
             private_key_file.read(),
             password=None,
             backend=default_backend()
         )

    plaintext = private_key.decrypt(
         ciphertext_byte,
         padding.OAEP(
             mgf=padding.MGF1(algorithm=hashes.SHA1()),
             algorithm=hashes.SHA1(),
             label=None
         )
    )
    return plaintext.split(":")

def make_bill_payment_request(bill):
    url = settings.API.get('billPayment').get('URL')
    terminal_pin = settings.API.get('billPayment').get('PIN')
    auth_url = settings.API.get('billPayment').get('Auth')
    callback_url = settings.API.get('IPG_Payment').get('Callback')
    client = Client(url)

    data = {}
    data['pin'] = terminal_pin
    data['orderId'] = int(mktime(datetime.now().timetuple()))
    data['callBackUrl'] = callback_url
    data['BillId'] = bill['BillID']
    data['PayID'] = bill['PayID']
    data['authority'] = 1
    data['status'] = 0

    auth_response = client.service.PinBillPaymentRequest(**data)
    authority_url = auth_url + str(auth_response['authority'])
    return auth_response['authority'], authority_url
    
