import time

from django.contrib.auth.models import User

from rest_framework import serializers

from models import UserProfile, Phone, UserPhones


class UserSerializer(serializers.ModelSerializer):
    id_number = serializers.CharField(read_only=True, source='userprofile.id_number')

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'id_number')


class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = ('user', 'id_number')


class PhoneSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    
    class Meta:
        model = Phone
        fields = ('username', 'number')


class UserPhoneSerializer(serializers.HyperlinkedModelSerializer):
    phone_number = serializers.CharField(source='phone.number')
    activation_timestamp = serializers.SerializerMethodField()

    def get_activation_timestamp(self, obj):
        return time.mktime(obj.activation_code_expiration.timetuple())

    class Meta:
        model = UserPhones
        fields = ('phone_number', 'active', 'verified', 'activation_timestamp')


class PhoneActivationSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=11)
    number = serializers.CharField(max_length=12)
    activation_code = serializers.CharField(required=False)
