from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view

from utils import get_bill_info, make_ipg_payment, verify_ipg_payment, pay_bill, decrypt_pin_pan, \
                        make_bill_payment_request, verify_bill_payment
from models import UtilTransaction
from utility.utils import adsl_get_status, adsl_charge, adsl_get_packages
from utility.models import Utility


# TODO csrf_exempt should be reimplemented
@csrf_exempt
def ipg_bill_payment(request):
    """
    Finding the bill again by getting all user's bills and
    preventing amount being changed by clients
    but for now we only have one bill

    :params username: user mobile number
    :params number: phone number
    """
    username = request.POST.get('username')
    bill = get_bill_info(request.POST.get('number'))
    au, payment_url = make_bill_payment_request(bill=bill)

    util = Utility.objects.get(en_name='bill')
    transaction = util.make_bill_transaction(username=username, bill=bill, au=au)
    
    context = {}
    context['bill'] = bill
    context['payment_url'] = payment_url
    context['transaction_id'] = transaction.id
    context['util'] = util
    context['payment'] = 'bill'
    
    return render(request, 'payment/invoice.html', context)

@csrf_exempt
def ipg_adsl_payment(request):
    """
    Buying ADSL packages
    For post-paid users, only call charge method
    For pre-paid users, do payment and call charge
    :params username: User mobile number
    :params number: Phone number
    :params package_id: Package ID to be bought
    """
    username = request.POST.get('username')
    number = request.POST.get('number')
    package_id = request.POST.get('package_id')
    price = adsl_get_packages(number)['errmsg'][0].get('price')
    au, payment_url = make_ipg_payment(amount=price)
    util = Utility.objects.get(en_name='adsl')
    transaction = util.make_adsl_transaction(username=username, number=number, amount=price, au=au)
    
    context = {}
    context['number'] = number
    context['payment_url'] = payment_url
    context['transaction_id'] = transaction.id
    context['price'] = price
    context['payment'] = 'adsl'
    
    return render(request, 'payment/invoice.html', context)


# TODO csrf_exempt should be reimplemented
@csrf_exempt
def ipg_verification(request):
    au = request.GET.get('au')
    rs = request.GET.get('rs')

    transaction = UtilTransaction.objects.get(au=au)

    if not transaction:
        raise ValidationError('Transaction not found')

    if verify_ipg_payment(authority=au, status=rs):
        return render(request, 'payment/ipg_success.html', {'transaction': transaction})

    return render(request, 'payment/ipg_failure.html', {'transaction': transaction})


@csrf_exempt
def bill_verification(request):
    au = request.GET.get('au')
    rs = request.GET.get('rs')

    transaction = UtilTransaction.objects.get(au=au)

    if not transaction:
        raise ValidationError('Transaction not found')

    if verify_bill_payment(authority=au, status=rs):
        return render(request, 'payment/ipg_success.html', {'transaction': transaction})

    return render(request, 'payment/ipg_failure.html', {'transaction': transaction})


class TwoFactorPayment(APIView):
    """
    Two factor bill payment

    :params username: user mobile number
    :params number: phone number
    :params pin2: user card pin2
    :params pan: user card pan
    """
    def post(self, request):
        username = request.data.get('username')
        bill = get_bill_info(request.data.get('number'))
        payload = request.data.get('payload')
        pan, pin2 = decrypt_pin_pan(payload)

        util = Utility.objects.get(en_name='bill')
        transaction = util.make_bill_transaction(username=username, bill=bill)

        result = pay_bill(mobile_number=username, bill_id=bill['BillID'], pay_id=bill['PayID'],
                        pin2=pin2, pan=pan)

        print result

        return Response(result)


@api_view(['POST'])
def in_app_purchase(request):

    if request.method == "POST":

        app = App.objects.get(en_name=request.data.get('app_name'))
        item = AppItem.objects.get(en_name=request.data.get('item'), app=app)
        transaction = item.make_app_transaction(username='admin')

        return Response({'transaction_id': transaction.id, 'amount': transaction.amount})
