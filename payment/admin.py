from django.contrib import admin

from models import PaymentType, UtilTransaction, AppTransaction


class PaymentTypeAdmin(admin.ModelAdmin):
    list_display = ['en_name', 'fa_name']


class UtilTransactionAdmin(admin.ModelAdmin):
    list_display = ['user', 'amount', 'status', 'rrn', 'date', 'payment_type', 'util']
    readonly_fields = ['user', 'amount', 'status', 'rrn', 'date', 'payment_type', 'util', 'au', 'pay_id', 'bill_id']
    search_fields = ['pay_id', 'bill_id']


class AppTransactionAdmin(admin.ModelAdmin):
    list_display = ['user', 'amount', 'status', 'rrn', 'date', 'payment_type', 'app_item']


admin.site.register(PaymentType, PaymentTypeAdmin)
admin.site.register(UtilTransaction, UtilTransactionAdmin)
admin.site.register(AppTransaction, AppTransactionAdmin)
