from django.conf.urls import url

from cp import views

urlpatterns = [
    url(r'^users/$', view=views.CpList.as_view()),
    url(r'^user/(?P<user__username>[\w]+)/$', view=views.CpDetail.as_view()),
]

