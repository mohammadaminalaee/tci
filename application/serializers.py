from rest_framework import serializers

from models import AppRating, ApplicationAsset, App, Campaign, Category


class AppRatingSerializer(serializers.ModelSerializer):

    class Meta:
        model = AppRating
        fields = ('id', 'app', 'rating', 'comment')


class AppAssetSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApplicationAsset
        fields = ('image',)


class AppSerializer(serializers.ModelSerializer):
    assets = AppAssetSerializer(many=True)

    class Meta:
        model = App
        fields = ('__all__')


class CampaignSerializer(serializers.ModelSerializer):
    app = serializers.HyperlinkedRelatedField(queryset=App.objects.all(), view_name='app-detail')

    class Meta:
        model = Campaign


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('__all__')